<!-- >> **NOTE:** This README.md file should be placed at the **root of each of your main directory.** -->

# Lis 4368 - Advanced Web Applications Development

## Jacob Roberts

### Lis 4368 Requirements:

*Course Work Links:*

1. [A1 README.md](a1/README.md "My A1 README.md file")
    - Install JDK
    - Install Tomcat
    - Provide screenshots of installations
    - Create Bitbucket repo
    - Complete Bitbucket tutorials
    - Provide git command descriptions

2. [A2 README.md](a2/README.md "My A2 README.md file")
    - Install MySQL
    - Create a WebbApp
    - Create Servlets
    - Install MySQL JDBC Driver 

3. [A3 README.md](a3/README.md "My A3 README.md file")
    - Create MySQL ERD model
    - Provide a3.sql file
    - Provide a3.mwb file

4. [A4 README.md](a4/README.md "My A4 README.md file")
    - Clone assignment student files
    - Modify files to add serverside validation
    - Compile class and servlet files

5. [A5 README.md](a5/README.md "My A5 README.md file")
    - Clone student starter files
    - Create serverside validation
    - Connect web application to database
    - Compile class and servlet files

6. [P1 README.md](p1/README.md "My P1 README.md file")
    - Clone assignment starter files
    - Add input fields
    - Confirm form validation

7. [P2 README.md](p2/README.md "My P2 README.md file")
    - Clone student starter files
    - Connect database to website
    - Add edit and delete functionality
    - Compile class and servlet files
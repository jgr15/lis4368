
# Lis 4368

## Jacob Roberts

### Project 1 Requirements:

*Requirements:*

1. Clone assignment starter files
2. Add form validation parameters
3. Chapter Questions(Ch 9-10)

#### README.md file should include the following items:

* Screenshot of failed validation
* Screenshot of passed validation
* Link to local host website



#### Assignment Screenshots and links:
Local Host Website
[http://localhost:9999/lis4368](http://localhost:9999/lis4368 "lis4368 home page link")

*Screenshots *:

Failed Validation 
![Failed Validation](img/Formvalidation.png "Failed Validation")
 
Passed Validation 
![Passed Validation](img/passedvalidation.png "Passed Validation")

> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4368 Advanced Web Applications Development

## Jacob Roberts

### Assignment 1 # Requirements:

*Three Parts:*

1. Distributed Version Control with Git and BitBucket
2. Java/JSP/Servlet Development Installation
3. Chapter Questions (Ch 1-4)

#### README.md file should include the following items:

* Screenshot of running java Hello (#1 above);
* Screenshot of running https//localhost9999 (#2 above, step #4/(b) in tutorial);
* git commands with short descriptions
* Bitbucket repo links: a) this assignment and b) the completed tutorial above

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>
> #### Git commands w/short descriptions:

1. git init- create a new local repository 
2. git status- lists the files youve changed and those you still need to change 
3. git add- add files 
4. git commit- adds a commit 
5. git push- Sends changes to master branch 
6. git pull- fetch changes on the remote server 
7. git grep- search the working directory for

#### Assignment Screenshots:

*Screenshot of AMPPS running http://localhost*:

![AMPPS Installation Screenshot](img/ampps.png)

*Screenshot of running java Hello*:

![JDK Installation Screenshot](img/jdk_install.png)

*Screenshot of Android Studio - My First App*:

![Android Studio Installation Screenshot](img/android.png)


#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/username/bitbucketstationlocations/ "Bitbucket Station Locations")

*Tutorial: Request to update a teammate's repository:*
[A1 My Team Quotes Tutorial Link](https://bitbucket.org/username/myteamquotes/ "My Team Quotes Tutorial")

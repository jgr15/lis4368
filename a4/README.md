
# Lis 4368

## Jacob Roberts

### Assignment #4 Requirements:

*Requirements:*

1. Clone student starter files
2. Add server-side validations
3. Compile class and servlet files
3. Chapter Questions(Ch 11-12)

#### README.md file should include the following items:

* Screenshot of server-side validation
* Link to local host site



#### Assignment Screenshot and Links:

Local Host Website
[http://localhost:9999/lis4368](http://localhost:9999/lis4368 "lis4368 home page link")


*Screenshot of server-side validation*:

Failed validation
![Failed Validation](img/failedval.png "Failed validation")
 
Passed Validation
![Passed Validation](img/passedval.png "Passed Validation")


# Lis 4368

## Jacob Roberts

### Assignment #5 Requirements:

*Requirements:*

1. Clone student starter files
2. Create serverside validation
3. Connect web application to database
4. Compile class and servlet files
5. Chapter Questions(Ch 13-15)

#### README.md file should include the following items:

* Valid user form entry screenshot
* Passed validation screenshot
* Associated database entry screenshot
* Link to local host site



#### Assignment Screenshot and Links:

Local Host Website
[http://localhost:9999/lis4368](http://localhost:9999/lis4368 "lis4368 home page link")


*Screenshot of server-side validation*:

Entry validation
![Server Validation](img/serverval.png "Server Validation")
 
Passed Server Validation
![Passed Seerver Validation](img/passedserverval.png "Passed Server Validation")

Associated Database Entry
![Associated Database Entry](img/associatedentry.png "Associated Database Entry")
> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# Lis 4368

## Jacob Roberts

### Assignment #2 Requirements:

*Requirements:*

1. Develop and Deploy a WebApp
2. Create a Java Servlet
3. Chapter Questions(Ch 5-6)

#### README.md file should include the following items:

* Assessment links
* Screenshots of working webpages


* [http://localhost:9999/hello/index.html](http://localhost:9999/hello/index.html "helloindex.html link")

* [http://localhost:9999/hello/sayhello](http://localhost:9999/hello/sayhello "sayhello link")

* [http://localhost:9999/hello/querybook.html](http://localhost:9999/hello/querybook.html "querybook link")

* [http://localhost:9999/hello/sayhi](http://localhost:9999/hello/sayhi "sayhi link")
* [http://localhost:9999/lis4368](http://localhost:9999/lis4368 "lis4368 home page link")


#### Assignment Screenshots:



*Screenshot of localhost hello/index.html*:
[http://localhost:9999/hello/index.html](http://localhost:9999/hello/index.html "helloindex.html link")
                
![hello/index.html Screenshot](img/helloindex.png)


>

*Screenshot of sayhello*:
[http://localhost:9999/hello/sayhello](http://localhost:9999/hello/sayhello "sayhello link")
                
![Sayhello Screenshot](img/sayhello.png)

>

*Screenshot of querybook.html*:
[http://localhost:9999/hello/querybook.html](http://localhost:9999/hello/querybook.html "querybook link")
                
![querybook.html Screenshot](img/querybook.png)

>

*Screenshot of sayhi*:
[http://localhost:9999/hello/sayhi](http://localhost:9999/hello/sayhi "sayhi link")
                
![sayhi Screenshot](img/sayhi.png)

>

*Screenshot of selected querybook*:
[http://localhost:9999/hello/querybook.html](http://localhost:9999/hello/querybook.html "selected querybook link")
                
![selected querybook Screenshot](img/selectedquerybook.png)


*Screenshot of selected queryresults*:

                
![querybook results Screenshot](img/queryresults.png)






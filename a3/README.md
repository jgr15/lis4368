
# Lis 4368

## Jacob Roberts

### Assignment #3 Requirements:

*Requirements:*

1. Download MySQL workbench
2. Create an ERD model that follows the business rules and forward engineers
3. Include data (atleast 10 records for each table)
3. Chapter Questions(Ch 7-8)

#### README.md file should include the following items:

* Screenshot of A3 ERD
* Links to a3.mwb



#### Assignment Screenshot and Links:
*Screenshot A3 ERD*:
![A3 ERD](img/a3.png "ERD based upon A3 Requirements")
*A3 docs: a3.mwb and a3.sql*: 

[A3 MWB File](docs/a3.mwb "A3 ERD in .mwb format")

[A3 SQL File](docs/a3.sql "A3 SQL Script")
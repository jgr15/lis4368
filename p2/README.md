
# Lis 4368

## Jacob Roberts

### Project 2 Requirements:

*Requirements:*

1. Clone assignment starter files
2. Add edit and delete functionality
3. Chapter Questions(ch 16 -17)

#### README.md file should include the following items:

* Screenshot of valid user
* Screenshot of passed validation
* Screenshot of modify form
* Screenshot of displayed data
* Screenshot of delete warning
* Screenshot of database changes
* Screenshot of modified data
* Link to local host website



#### Assignment Screenshots and links:
Local Host Website
[http://localhost:9999/lis4368](http://localhost:9999/lis4368 "lis4368 home page link")

*Screenshots *:

Valid User
![Valid User](img/validuser.png "Valid User")
 
Passed Validation 
![Passed Validation](img/passedval.png "Passed Validation")

Data Display
![Data Display](img/displaydata.png "Data Display")

Modify Form 
![Modify Form](img/modifyform.png "Modify Form")

Modified Data
![Modified Data](img/modifieddata.png "Modified Data")

Delete Warning 
![Delete Warning](img/deletewarning.png "Delte Warning")

Database Changes 
![Database Changes](img/databasechanges.png "Database Changes")
